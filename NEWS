0.4.0
=====

* New build system
* Continue development

0.3.12
======

* Remove markup from UI files (Daniel Mustieles)
* Fix format not a string literal (Bart Martens)
* Remove unused variables (Claudio Saavedra)

New and updated translatiosn:

cs: Marek Černocký
es: Daniel Mustieles
pl: Piotr Drąg
pt_BR: Rafael Ferreira
sl: Matej Urbančič
sr: Мирослав Николић (sr)
sr@latin: Мирослав Николић

0.3.11
======

* Linking fixes (Claudio Saavedra)

New and updated translations:

cs: Marek Černocký
de: Mario Blättermann
es: Daniel Mustieles, Jorge González
fr: Bruno Brouard
gl: Fran Dieguez
pl: Piotr Drąg
pt_BR: Mateus Zenaide
ro: Lucian Adrian Grijincu, Alexandru Baicoianu
sl: Andrej Žnidaršič
sv: Daniel Nylander

0.3.10
======

* Replace glade with gtkbuilder (Alejandro Valdés, Claudio Saavedra)
* Replace gnet with gio (Claudio Saavedra)
* Remove GTK+ deprecated API usage (Claudio Saavedra)
* Memory leaks fixed, UI improvements, and code fixes (Claudio Saavedra)
* autotools and intltool fixes (Claudio Saavedra, Claude Paroz)

New and updated translations:

da: Joe Hansen
fr: Bruno Brouard
ga: Dario Villar Veres, Fran Diéguez
hu: Gabor Kelemen
pl: Piotr Drąg
sl: Andrej Žnidaršič, Matej Urbančič
zh_CN: Aron Xu, du baodao

0.3.9
=====

* Fix build
* Minor code cleanup

New and updated translations:

cs: Marek Černocký
de: Mario Blättermann
sl: Andrej Žnidaršič
sv: Daniel Nylander

0.3.8
=====

* Remove dependency on libgnomeui (Francisco Rojas).
* Remove dependency on libgnomeprintui.
* Fix a crasher on 64 bits machines (Francisco Rojas).
* Code cleanups and fixes.

Bug fixes:

#435869, crash in Gyrus IMAP Cyrus Administrator: connecting to a
 server w... (Francisco Rojas)
#555919, remove dependency on libgnomeui (Francisco Rojas)

New and updated translations:

ar: Djihed Afifi
cs: Petr Kovar, Lucas Lommer, Andre Klapper
en_GB: David Lodge
es: Jorge González
nb: Kjartan Maraas
oc: Yannig Marchegay (Kokoyaya)
pt_BR: Flamarion Jorge, Vladimir Melo
sv: Daniel Nylander

0.3.7
=====

* Many usability fixes, dialogs clean up, more GTK+ stock usage, etc.
* Show the program version in bug-buddy.
* Build system fixes.
* Make the tests build optional (disabled by default).
* Make gnutls requirement optional (disabled by default).
* Logo updated (Alejandro Valdés).
* Fixed a typo (Bob Mauchin).
* Many GObject related fixes and refactoring (Alejandro Valdés, Claudio).
* GyrusConnection class works with secure connections.
* Allow login with passwords with special characters.
* Several code cleanups.

Bug fixes:

#337655 -- Allow passwords with special characters.
#319436 -- Use correctly gnutls_record_recv () when it needs more than 1024 
	   bytes.
#429160 -- Logo shows old gyrus version.
#432628 -- Typo in src/gyrus-admin-mailbox.c
#429138 -- Private data of GObject classes are not hidden.

New and updated translations:

ar: Djihed Afifi
ca: Jordi Deu-Pons
en_GB: David Lodge
es: Francisco Javier F. Serrador 
fr: Robert-André Mauchin and Claude Paroz
dz: Sonam Pelden
fi: Ilkka Tuohela 
hu: Albitz Nóra
sv: Daniel Nylander
vi: Clytie Siddall

0.3.6
=====

UI:
* Dropped dependence on libbonoboui. Using now GtkUIManager.
* Allow to create ACL entries in mailboxes with empty ACL.
* Fixed typos and messages improved. (Clytie Siddall).

New Features:
* Allow to list and delete orphaned mailboxes.
* New secure connection client interface (still in development, gyrus not 
  ported to it) (Mario Fuentes).

Fixes:
* Fixed memory leaks.
* Use automake 1.9 and some configure and build fixes (#327767, chpe@gnome.org).

New and updated translations:
* Lukáš Novotný (cs).
* Adam Weinberger (en_CA).
* Francisco Javier F. Serrador (es).
* Ilkka Tuohela (fi).
* Vincent van Adrighem (nl).
* Clytie Siddall (vi).

Thanks to:

chpe@gnome.org, Mario Fuentes, Lukáš Novotný, Francisco Javier F. Serrador, 
Clytie Siddall, Ilkka Tuohela, Vincent van Adrighem, Adam Weinberger.


0.3.5
=====

UI:
* Removed unused images from distribution.
* Improved some messages.
* Better usage of GTK+ stock icons.

Fixes:
* Do not crash when manipulating old format sessions (#314809).
* Fixed several memory leaks and compilation time warnings.

Translations:
* New and updated translations (en_CA, vi).

Thanks to:

Mauricio Fuentes, Clitye Siddall, Adam Weinberger.

0.3.4
=====

New Features:
* Allows to create printable reports of users with quota exceeded (Alejandro
  Valdés).
* Added support for [ALERT] server responses.
* Added support for servers using the UNIX mailbox hierarchy separator instead
  of the netnews separator character.

UI:
* Shows the name of the active session in the title of the main window (Héctor 
  Enríquez Díaz, Claudio).
* Better usage of GTK+ stock icons (Mario Fuentes, Pedro Villavicencio 
  Garrido).
* Improved dialog for edition of sessions (Mario, Claudio).
* Dumped to GTK+ 2.6.0 and using GtkDialogAbout (Pedro).

Translations:
* Updated tarnslations (en_CA, es).
* New translations (uk).

Bug fixes:
* #308422. Check if iter is set correctly (Juan Carlos Inostroza).
* #305197. Do not hang when server uses [ALERT] responses.

Thanks to:

Héctor Enríquez Díaz, Maxim Dziumanenko, Mario Fuentes, Juan Carlos Inostroza, 
Alejandro Valdés, Pedro Villavicencio Garrido, Adam Weinberger.

0.3.3
=====

UI:
* Changed GtkEntry'es to GtkLabel's in main interface.
* Removed password entry and changed for a dialog.

Integration:
* Added entry in GNOME Menu (Pedro Villavicencio).

Translations:
* Updated tarnslations (en_CA, es).
* New translations (en_GB).

Bug fixes:
* #303402. Added bind_textdomain_codeset() call (Pedro).
* #301960. Reject utf-8 identifiers in ACL.

Thanks to:

Pedro Villavicencio, Gareth Owen, Adam Weinberger.

0.3.2
=====

Access Control List:
* Allows to add, remove and rename ACL entries.
* Allows to modify permissions.
* Hides Access Control List when user doesn't have permission to see it.

UI:
* Improved messages and fixed typos.
* Various improvements in GUI.
* Added menu items for ACL manipulation.

Translations:
* Translation now works.
* New translations (en_CA pt_BR sv zh_CN).
* Updated translations (es).

Bug fixes:
* Allows to create mailboxes under mailboxes whose name include spaces.
* Show correctly permission 'write' in the ACL.
* Do not crash if user doesn't have permission to  see ACL.
* Better interaction for non administrator users.

Thanks to:

Christian Rose, Funda Wang, Adam Weinberger, Raphael Higino, Rodrigo Moya, 
Fernando San Martín W.

0.3.1
=====

* A Dialog for finding mailboxes.
* Shows the Access Control List (ACL) for selected mailboxes. Next 
  releases will include the ability to modify ACL and add/remove 
  entries.

Bug Fixes:

* Removed some compile time warnings (Claudio, Casper Pedersen).
* Allows to create mailboxes in root level.
* Show correct messages when user has no permission to check the 
  quota of a mailbox.

i18n: 

* Updated translation (es).

0.3.0
=====

Added following modules:

* Creation of mailboxes.
* Deletion of mailboxes.

Improved GUI:

* Added a toolbar.
* Improved messages.

i18n:

* Updated translation (es).
* Added intltool to help with translations.
* Improved structure of some messages.

0.2.99
======

* Fixed GConfs bugs.
* Improved sessions interface.
* Allow modification of user quotas.
