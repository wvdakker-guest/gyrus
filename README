Gyrus: GNOME IMAP/Cyrus Administrator.
======================================

https://gitlab.com/wvdakker/gyrus

Version: 0.4.0

Gyrus is a small tool for the administration of mailboxes in
IMAP/Cyrus servers. The main idea behind it, is to provide mail server
administrators with a better way to do the daily maintenance than a
command line or a plain and boring telnet client..

Gyrus is free software and licensed under the GPL v.2, you are free and
encouraged to copy it and distribute it. See the COPYING file for details.

* Getting Gyrus

Gyrus can be downloaded from its Web page: http://www.gnome.org/projects/gyrus. 
If you want to get the development version, you can check it out from
the git repository:

  $ git clone https://gitlab.com/wvdakker/gyrus

Normally, the development version is pretty stable, so don't feel too scared to
download it.

* Dependencies

The needed dependencies include:

gtk+-3.0 (>= 3.22.0)
gio (>= 2.22.0)
amtk(>= 5.5)

* Features

  - Connections:

    Currently, gyrus support only plain IMAP connections (normally, these
    go by port 143). In the future, secure connections will be implemented.

    Also, servers using the UNIX hierarchy separator are supported.

  - Mailboxes:

    Gyrus allows you to browse through all the mailboxes of the server. For
    a better performance, you can search a desired mailbox with the 'Find'
    dialog.

    You can create mailboxes: for new users, or as sub-mailboxes for existing
    ones.

    Also, if you have the needed permissions, you can delete mailboxes easily.

  - Quota Management:

    With Gyrus you can define the Quota of mailboxes (ie. the maximum amount
    of space resources that a user can use in its mailboxes).

    It is possible to create printable reports of mailboxes whose quota is
    over a percentage, so administrator can easily monitor mailboxes
    with quota exceeded.

  - Access Control List:

    Gyrus lets you administrate the ACL of each mailbox, i.e., to create,
    delete, remove and modify the entries of Access Control List.

* Contributing:

For things that need to be worked you can check the TODO file that should
come with any gyrus distribution.

If you want to send bug reports, patches, improvements, suggestions, please
use https://gitlab.com/wvdakker/gyrus.

Instructions to subscribe to the list follow.

* Credits:

  - Maintainer and main developer:

  Claudio Saavedra <csaavedra@igalia.com>

  - Contributors:

  Francisco Rojas <frojas@alumnos.utalca.cl>

  - Original author:

  Alejandro Valdés <avaldes@utalca.cl>

  - Former Contributors:

  Jorge Bustos <jbustos@utalca.cl>
  Juan Carlos Inostroza <jci@tux.cl>
  Mario Fuentes <mario@gnome.cl>
  Felipe Barros <felipe@linuxlab.cl>

* Known Bugs:

  - When the inactivity time is reached and the server sends a "BYE LOGOUT"
    command and disconnects, Gyrus 'can' hang out.

  - Even when it's not a bug, gyrus still can't handle secure connections.
    Please do not report this as a bug, we are working on it.
