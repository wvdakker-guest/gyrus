Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Gyrus

Files: *
Copyright: 1995-1997, Ulrich Drepper <drepper@gnu.ai.mit.edu>
           1998-2010, Free Software Foundation, Inc
           2003-2004, Jorge Bustos Bustos <jbustos@utalca.cl>
           2003-2005, GNOME Foundation, Inc
           2003-2005, Alejandro Valdés <avaldes@utalca.cl>
           2003-2007, Claudio Saavedra <csaavedra@alumnos.utalca.cl>
           2004-2007, Free Software Foundation, Inc
           2004-2008, Rodney Dawes <dobey.pwns@gmail.com>
           2005-2006, Adam Weinberger and the GNOME Foundation
           2005-2009, the author(s) of Gyrus
           2006, Ilkka Tuohela
License: GPL-2+

Files: debian/*
Copyright: 2005-2013 Ondřej Surý <ondrej@debian.org>
           2019 Yavor Doganov <yavor@gnu.org>
           2022 Willem van den Akker <wvdakker@wilsoft.nl>
License: GPL-2+

Files: debian/gyrus.1
Copyright: 2019, Yavor Doganov <yavor@gnu.org>
License: FSFAP

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or (at
 your option) any later version.
 .
 This package is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 .
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in `/usr/share/common-licenses/GPL-2'.

License: Permissive
 This file may be copied and used freely without restrictions.  It may
 be used in projects which are not available under a GNU License, but
 which still want to provide support for the GNU gettext functionality.

License: FSFAP
 Copying and distribution of this file, with or without modification,
 are permitted in any medium without royalty provided the copyright
 notice and this notice are preserved.
