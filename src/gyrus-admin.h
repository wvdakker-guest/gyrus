/*
  gyrus-admin.h

  GYRUS -- GNOME Cyrus Administrator. Administrator Object.
  
  Copyright (C) 2004-2005 Claudio Saavedra V. <csaavedra@alumnos.utalca.cl>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GYRUS_ADMIN_H
#define GYRUS_ADMIN_H

#include <gtk/gtk.h>
#include "gyrus-session.h"

#define GYRUS_TYPE_ADMIN            (gyrus_admin_get_type ())
#define GYRUS_ADMIN(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GYRUS_TYPE_ADMIN, GyrusAdmin))
#define GYRUS_ADMIN_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GYRUS_TYPE_ADMIN, GyrusAdminClass))
#define GYRUS_IS_ADMIN(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GYRUS_TYPE_ADMIN))
#define GYRUS_IS_ADMIN_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GYRUS_TYPE_ADMIN))
#define GYRUS_ADMIN_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  GYRUS_TYPE_ADMIN, GyrusAdminClass))

typedef struct _GyrusAdminPrivate GyrusAdminPrivate;
typedef struct _GyrusAdmin        GyrusAdmin;
typedef struct _GyrusAdminClass   GyrusAdminClass;

/*** Gyrus Main Application Data ***/
struct _GyrusAdmin {
	GtkVBox vbox;
	GyrusAdminPrivate *priv;
};

/*** Gyrus Main Application Class Data ***/
/*** derived from GtkVboxClass ***/

struct _GyrusAdminClass {
	 GtkVBoxClass vbox_class;
};

typedef enum {
	COL_MAILBOX_ICON,
	COL_MAILBOX_BASENAME,
	COL_MAILBOX_NAME,
	COL_MAILBOX_NUMBER
} GyrusColumnMailbox;

typedef enum {
	COL_ACL_IDENTIFIER,
	COL_ACL_RIGHT_L,
	COL_ACL_RIGHT_R,
	COL_ACL_RIGHT_S,
	COL_ACL_RIGHT_W,
	COL_ACL_RIGHT_I,
	COL_ACL_RIGHT_P,
	COL_ACL_RIGHT_C,
	COL_ACL_RIGHT_D,
	COL_ACL_RIGHT_A,
	COL_ACL_MODIFIED,
	COL_ACL_NUMBER
} GyrusColumnAcl;

typedef enum {
	GYRUS_IMAP_STATUS_BAD,
	GYRUS_IMAP_STATUS_BYE,
	GYRUS_IMAP_STATUS_NO,
	GYRUS_IMAP_STATUS_OK,
	GYRUS_IMAP_STATUS_LIST
} GyrusImapStatus;	

typedef enum {
	GYRUS_ADMIN_LOGIN_NO_PASS,    /* it is supposed this never happens */
	GYRUS_ADMIN_LOGIN_BAD_LOGIN,  /* wrong user or password            */
	GYRUS_ADMIN_LOGIN_NO_HOST,    /* Problems when connecting to host  */
	GYRUS_ADMIN_LOGIN_OK          /* logged in                         */
} GyrusAdminLoginError;	

GType gyrus_admin_get_type (void);

/**
   Creates a new instance of #GyrusAdmin. This admin
   will be set to work with the #GyrusSession that
   you must provide as argument.

   @session: A previously initialized #GyrusSession.

   Returns: A new #GyrusAdmin.
   
*/
GtkWidget * gyrus_admin_new (GyrusSession *session);

/**
   Gets the @host where the #GyrusAdmin is connected.
*/
const gchar *gyrus_admin_get_current_host (GyrusAdmin *admin);

/**
   Gets the @user that is connected in #GyrusAdmin.
*/
const gchar *gyrus_admin_get_current_user (GyrusAdmin *admin);


/**
   Gets the name of the session.
*/
const gchar *gyrus_admin_get_current_session_name (GyrusAdmin *admin);

/**
   Gets the password that the #GyrusAdmin is using to connect to 
   the server.
*/
const gchar *gyrus_admin_get_current_passwd (GyrusAdmin *admin);

/**
   Gets the port where the #GyrusAdmin is connected.
*/
int gyrus_admin_get_current_port (GyrusAdmin *admin);

/**
Gets the separator character of the administrator of the server.
 */
const gchar*
gyrus_admin_get_separator_char (GyrusAdmin *admin);

/**
   Returns whether @admin is or not connected to the server.
*/
gboolean gyrus_admin_is_connected (GyrusAdmin *admin);

/**
 * Returns TRUE if the user that is connected has access to the
 * ACL of currently selected mailbox. If there is no mailbox
 * selected, then returns FALSE.
 */
gboolean gyrus_admin_has_current_acl_access (GyrusAdmin *admin);

void gyrus_admin_logged_out (GyrusAdmin *admin);
void gyrus_admin_refresh_users_list (GyrusAdmin *admin);
gchar *gyrus_admin_get_selected_mailbox (GyrusAdmin *admin);
void gyrus_admin_select_mailbox (GyrusAdmin *admin, const gchar *mailbox);

/**
   Listen the channel of @admin and allocates the message in @message.
   If @msg_len is not NULL, will store there the size 
   of the allocated message.

   @admin: A #GyrusAdmin object.
   @message: Allocated message.
   @msg_len: If not NULL, the size of the allocated message.

   Returns: The status of the connection.
*/
GyrusImapStatus gyrus_admin_listen_channel (GyrusAdmin *admin, gchar **message,
					    gint *msg_len);

/**
   Write @message in the channel of @admin. @message should be a 
   valid IMAP command, following the RFC. To read the response of 
   the server, use gyrus_admin_listen_channel ().

   Returns: %TRUE if successful, %FALSE otherwise
*/
gboolean gyrus_admin_write_channel (GyrusAdmin *admin, gchar *message);

/**
   Parses a LIST server message and get the mailbox name from it.

   @msg: The message returned by the server.

   Returns: a newly allocated mailbox name.
*/
gchar *
gyrus_admin_get_mailbox_from_list_message (gchar *msg);

/**
   Returns the treeview with the mailboxes. You need to
   unref it once you are done with it.
*/
GtkTreeView * gyrus_admin_get_users_treeview (GyrusAdmin *admin);

#endif /* GYRUS_ADMIN_H */

