/*
  gyrus-dialog-mailbox-new.h

  GYRUS -- GNOME Cyrus Administrator. Dialog New Mailbox.

  Copyright (C) 2004 Claudio Saavedra Valdés <csaavedra@alumnos.utalca.cl>
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, see <http://www.gnu.org/licenses/>.
  
*/

#ifndef GYRUS_DIALOG_MAILBOX_NEW_H
#define GYRUS_DIALOG_MAILBOX_NEW_H

#define GYRUS_TYPE_DIALOG_MAILBOX_NEW            (gyrus_dialog_mailbox_new_get_type ())
#define GYRUS_DIALOG_MAILBOX_NEW(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GYRUS_TYPE_DIALOG_MAILBOX_NEW, GyrusDialogMailboxNew))
#define GYRUS_DIALOG_MAILBOX_NEW_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GYRUS_TYPE_DIALOG_MAILBOX_NEW, GyrusDialogMailboxNewClass))
#define GYRUS_IS_DIALOG_MAILBOX_NEW(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GYRUS_TYPE_DIALOG_MAILBOX_NEW))
#define GYRUS_IS_DIALOG_MAILBOX_NEW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GYRUS_TYPE_DIALOG_MAILBOX_NEW))
#define GYRUS_DIALOG_MAILBOX_NEW_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  GYRUS_TYPE_DIALOG_MAILBOX_NEW, GyrusDialogMailboxNew))

typedef struct _GyrusDialogMailboxNewPrivate GyrusDialogMailboxNewPrivate;
typedef struct _GyrusDialogMailboxNew        GyrusDialogMailboxNew;
typedef struct _GyrusDialogMailboxNewClass   GyrusDialogMailboxNewClass;

struct _GyrusDialogMailboxNew  {
	GtkDialog dialog;
	GyrusDialogMailboxNewPrivate *priv;
};

struct _GyrusDialogMailboxNewClass {
	 GtkDialogClass dialog_class;
};

GType gyrus_dialog_mailbox_new_get_type (void);

/** 
    Creates a dialog to allow the creation of new 
    mailbox.

    Returns: The dialog for creating new mailboxes.
*/
GtkWidget *gyrus_dialog_mailbox_new_new (void);

#endif /* GYRUS_DIALOG_MAILBOX_NEW_H */
